import smbus as I2C
import time
import filtros

class ADC(object):
    """Reads values from the pH probe + MinipH and converts to pH
    """
    def __init__(self, addr=0x4e, busnum=-1):
        """Creates an pH Reader with a given address and on a specific I2C bus
        (if provided). If no bus number is provided, the default Adafruit_I2C
        bus will be selected (I2C2).
        :param addr: The address of the device on the I2C bus. Use i2cdetect -y -r 0 and
        i2cdetect -y -r 1 to probe for connected devices.
        :param busnum: Optional. The I2C bus number that the probe is on.
        Default Adafruit_I2C bus (I2C2) used if not specified.
        """
        self.addr = addr
        self.i2c = I2C.SMBus(1)
        try:
            self.value = self.read()
        except Exception as e:
            print("No hay ADC disponible")
            self.value = False


    def read(self):
        """
        :return : Integer between 0 and 4096 (2^12).
        """
        reading = self.i2c.read_i2c_block_data(self.addr, 0x00, 2)
        return (reading[0] << 8) + reading[1]


    def get_Voltage(self):
        """
        :return: Voltage
        """
        valorAnterior = self.value
        self.value = self.read()
        self.value = filtros.Filtro_pasa_bajas(self.value, valorAnterior)
        return round(self.value * 5.0 / 4096.0, 2)

    def get_bat(self, max=4.1, min=3.3):
        """
        calcula el porcentaje de la bateria conectada al adc
        :param max: valor maximo en voltaje de la bateria
        :param min: Valir minimo en voltaje de la bateria
        :return: % de bateria
        """
        voltage = self.get_Voltage()
        bat = ((voltage-min)/(max-min))*100
        if bat > 100:
            bat = 100
        return int(bat)
