# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#           Libreria notificacion de estados de bateria
#####################################################################
__author__ = 'cristianrojas'
import RPi.GPIO as GPIO
import credentials
import funciones


estoma = credentials.Get_Estoma_Info()[0]
estomaInfo = credentials.Get_Estoma_Info()

print(estoma)


# Setear GPIO en configuracion Broadcom y definir los pines de entrada
GPIO.setmode(GPIO.BCM)
cargador = 16
descarga = 23
# Setear el modo de operacion de los pines
GPIO.setwarnings(False)
GPIO.setup(cargador, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(descarga, GPIO.IN, pull_up_down=GPIO.PUD_UP)


def Carga(cargador):
        print("Deconexion")
        if GPIO.input(cargador) == 0:
            funciones.Web_conex("cargador", estoma)
            print("Se desconecto el cargador")


GPIO.add_event_detect(cargador, GPIO.FALLING, callback=Carga, bouncetime=300)


