# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#           Funciones para captacion de racimos Sigma
#####################################################################
__author__ = 'cristianrojas'
######               Importacion de librerias                 #######
import time, datetime
import psycopg2, psycopg2.extensions, psycopg2.extras
import json, urllib.request, urllib.parse, urllib.error, requests
import RPi.GPIO as GPIO
import credentials
import socket
from subprocess import PIPE, Popen
from collections import OrderedDict
import os


####### Variables de identificacion
ESTOMAINFO = credentials.Get_Estoma_Info()
######            Lectura de parametros locales               #######
f = open("/firmware/base/Codes/initbascula/info.txt", "r")
parametrosLocales = str.split(f.readline(), ',')
f.close()
ESTOMAID = parametrosLocales[0]
MAXINTENTOSCHECKFLAG = 3
DEV = False

#####################################################################
#####################################################################
######                        Funciones                       #######
################# Funcion de actualizacion de la hora
def Actualizar_hora(dia=0):

    if dia == 1:
        Date = datetime.datetime.now()
        Date = Date.date()
        return Date
    else:
        Hours = datetime.datetime.now().strftime('%H:%M:%S')
        Date = datetime.datetime.now().strftime('%Y-%m-%d')
        fecha_peso = Date + " " + Hours
        return fecha_peso


###### Funcion de creacion de cursor
def Create_cursor(json=False):
    if json:
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')
        conectar.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_REPEATABLE_READ)
        cursor = conectar.cursor(cursor_factory=psycopg2.extras.DictCursor)
    else:
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')
        conectar.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_REPEATABLE_READ)
        cursor = conectar.cursor()
    return cursor, conectar


### Funcion que realiza la actualizacion en la base de datos local
def Update_db(peso, estado, fecha, viajeId, cantidad, serial, pesovastago):
    print("Actualizando db interna...")
    peso = peso
    errorPeso = Get_parametro('error')
    if peso == "":
        peso = 0
    fecha_peso = fecha  # Actualizar la fecha a almacenar
    cursor, conectar = Create_cursor()
    edad = Get_parametro_estoma("semana_default")
    try:
        # se realiza la sentencia SQL registrando la bandera de subido en 0
        cursor.execute("insert into racimitos (fecha, peso, estado, viaje_id, datos, error_peso, edad, serial, vastago) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       (fecha_peso, peso, estado, viajeId, cantidad, errorPeso, edad, serial, pesovastago))
        print("BD actualizada")

    except psycopg2.Error as e:
        print("error al actualizar la bd interna: ", e)

    conectar.commit()
    cursor.close()
    conectar.close()
    Set_barcadillero_default(viaje_id=viajeId)
    Set_operario_default(viaje_id=viajeId)


def Get_trama(tiempoSubida=30, eliminado=False, calibracions=False):
    try:
        if eliminado:
            Set_fecha_final()
            cursor, conectar = Create_cursor(json=True)
            conectar.commit()
            cursor.execute("select * from eliminacion_viajes where estado=0")
            recs = cursor.fetchall()
            rows = [dict(rec) for rec in recs]
            for row in rows:
                row['fecha'] = str(row['fecha'])
                row['fecha_final'] = str(row['fecha_final'])
                row['fecha_eliminacion'] = str(row['fecha_eliminacion'])
                viaje_id = row['viaje_id']
                cursor.execute("select * from eliminacion_racimitos where viaje_id = (%s)", (viaje_id,))
                racimitos = cursor.fetchall()
                racimitos = [dict(racimito) for racimito in racimitos]

                for racimo in racimitos:
                    racimo['fecha'] = str(racimo['fecha'])
                row['racimitos'] = racimitos

            if len(rows) > 0:
                print("Generando Json...")
                print('Valores seleccionados:')
                rows_json = json.dumps(rows)
                print(rows)
                return rows_json
            else:
                return 0
        elif calibracions:
            cursor, conectar = Create_cursor(json=True)
            conectar.commit()
            cursor.execute("select * from validacions where estado=0")
            recs = cursor.fetchall()
            rows = [dict(rec) for rec in recs]
            for row in rows:
                row['fecha'] = str(row['fecha'])
            if len(rows) > 0:
                print("Generando Json...")
                print('Valores seleccionados:')
                rows_json = json.dumps(rows)
                print(rows)
                return rows_json
            else:
                return 0
        else:
            Set_fecha_final()
            cursor, conectar = Create_cursor(json=True)
            conectar.commit()
            date = datetime.datetime.now() - datetime.timedelta(seconds=tiempoSubida*60)
            cursor.execute("select * from viajes where estado=0 and fecha_revisado <= (%s)", (date,))
            recs = cursor.fetchall()
            rows = [dict(rec) for rec in recs]
            for row in rows:
                row['fecha'] = str(row['fecha'])
                row['fecha_final'] = str(row['fecha_final'])
                row['fecha_revisado'] = str(row['fecha_revisado'])
                viaje_id = row['viaje_id']
                cursor.execute("select * from racimitos where viaje_id = (%s)", (viaje_id,))
                racimitos = cursor.fetchall()
                racimitos = [dict(racimito) for racimito in racimitos]
                for racimo in racimitos:
                    racimo['fecha'] = str(racimo['fecha'])
                row['racimitos'] = racimitos
                cursor.execute("select * from rechazos where viaje_id = (%s)", (viaje_id,))
                rechazos = cursor.fetchall()
                rechazos = [dict(rechazo) for rechazo in rechazos]
                for rechazo in rechazos:
                    rechazo['fecha'] = str(rechazo['fecha'])
                row['rechazos'] = rechazos
                cursor.execute("select * from perfil_racimos where viaje_id = (%s)", (viaje_id,))
                perfiles = cursor.fetchall()
                perfiles = [dict(perfil) for perfil in perfiles]
                row['perfil_racimos'] = perfiles

            if len(rows) > 0:
                print("Generando Json...")
                print('Valores seleccionados:')
                rows_json = json.dumps(rows)
                print(rows_json)
                return rows_json
            else:
                return 0

    except Exception as e:
        print("ERROR OBTENIENDO TRAMA: ", repr(e))

############# Funcion para realizar la conexion a la web
def Web_conex(tabla, datos, timeout=300):
    SiomappCredentials = credentials.Get_Siomapp_Credentials()
    mydata = [('s', SiomappCredentials[0]), ('m', tabla), ('d', datos)]
    URL = 'https://banapeso.siomapp.com/racimitos/subir.php'
    header = {"Content-type": "application/x-www-form-urlencoded"}
    if tabla == 'viajes':
        print("Se enviara un viaje")
        if DEV:
            URL = 'https://banapeso.sioma.dev/estomas/subirViajes.php'
        else:
            URL = 'https://banapeso.siomapp.com/estomas/subirViajes.php'
    elif tabla == 'viajes_eliminados':
        if DEV:
            URL = 'https://banapeso.sioma.dev/estomas/subirViajes.php?eliminado'
        else:
            URL = 'https://banapeso.siomapp.com/estomas/subirViajes.php?eliminado'
    elif tabla == 'calibracions':
        if DEV:
            URL = 'https://banapeso.sioma.dev/estomas/calibracion.php'
        else:
            URL = 'https://banapeso.siomapp.com/estomas/calibracion.php'
    mydata = [('s', SiomappCredentials[0]), ('data', datos)]
    mydata = urllib.parse.urlencode(mydata)
    try:
        page = requests.post(URL, data=mydata, headers=header, timeout=timeout)
        #print page.content
        return page.json()
    except Exception as e:
        print("ERROR EN CONEXION WEB: " + repr(e))
        return 0


def Actualizar_tabla(tabla, timeout=10):
    print("Tabla a sincronizar: ", tabla)
    SiomappCredentials = credentials.Get_Siomapp_Credentials()
    cursor, conectar = Create_cursor()
    execute = "SELECT column_name FROM information_schema.columns" \
              " WHERE table_schema = 'public' AND table_name = " + str("'" + tabla + "'")
    cursor.execute(execute)
    aux = cursor.fetchall()
    campos = []

    for campo in aux:
        campos.append(campo[0])

    aux = ""
    aux2 = ""

    for campo in campos:
        if campo == campos[0]:
            aux = campo
            aux2 = "%s"
        else:
            aux = aux + ", " + campo
            aux2 = aux2 + ", %s"
    insert = "insert into " + str(tabla) + " (" + aux + ") values (" + aux2 + ")"
    mydata = [('s', SiomappCredentials[0]), ('serial', ESTOMAID), ('tabla', tabla), ('campos', json.dumps(campos))]
    mydata = urllib.parse.urlencode(mydata)
    if DEV:
        URL = 'https://banapeso.sioma.dev/estomas/bajarTabla.php'
    else:
        URL = 'https://banapeso.siomapp.com/estomas/bajarTabla.php'
    header = {"Content-type": "application/x-www-form-urlencoded"}

    try:
        page = requests.post(URL, data=mydata, headers=header, timeout=timeout)
        recs = page.json(object_pairs_hook=OrderedDict)
        existe = False
        cursor.execute("delete from " + tabla)
        conectar.commit()

        for row in recs:
            datos = []
            for columna in campos:
                datos.append(row[columna])
            cursor.execute(insert, datos)

        conectar.commit()
        print("Tabla ", tabla, " actualizada")

    except Exception as e:
        print("ERROR ACTUALIZANDO TABLAS: " + repr(e))
        return 0

### Funcion que realiza la conexion con el servidor web y realiza el envio del JSON
def Sync():
    tiempoSubida = Get_parametro_estoma("tiempo_subida")
    #### Envio de Viajes
    base = Get_trama(tiempoSubida)
    try:
        if base is not 0:
            if base != "[]":
                response = Web_conex("viajes", base)
                if response != 0:
                    print(response)
                    keys = []
                    flag = 0
                    respuestas = response[0]
                    for respuesta in response:
                        respuestas.update(respuesta)
                    for key in list(respuestas.keys()):
                        if key == 'error':
                            flag = 1
                        keys.append(key)
                    if flag == 1:
                        print("Error de request: ", respuestas['error'])
                    else:
                        cursor, conectar = Create_cursor()
                        for viaje in keys:
                            # Actualizar la bandera de subido a los datos que se han ingresado a la web
                            cursor.execute("update viajes set estado=(%s) where estado=0 and viaje_id = (%s)", (respuestas[viaje], viaje))
                            cursor.execute("update racimitos set estado=(%s) where estado=0 and viaje_id = (%s)", (respuestas[viaje], viaje))
                            cursor.execute("update rechazos set estado=(%s) where estado=0 and viaje_id = (%s)", (respuestas[viaje], viaje))
                            conectar.commit()
    except Exception as e:
        print("ERROR SINCRONIZANDO: " + repr(e))

    ### Envio de viajes eliminados
    base = Get_trama(tiempoSubida, eliminado=True)
    try:
        if base is not 0:
            if base != "[]":
                response = Web_conex("viajes_eliminados", base)
                if response != 0:
                    print(response)
                    keys = []
                    flag = 0
                    respuestas = response[0]
                    for respuesta in response:
                        respuestas.update(respuesta)
                    for key in list(respuestas.keys()):
                        if key == 'error':
                            flag = 1
                        keys.append(key)
                    if flag == 1:
                        print("Error de request: ", respuestas['error'])
                    else:
                        cursor, conectar = Create_cursor()
                        for viaje in keys:
                            # Actualizar la bandera de subido a los datos que se han ingresado a la web
                            cursor.execute("update eliminacion_viajes set estado=(%s) where estado=0 and viaje_id = (%s)",
                                           (respuestas[viaje], viaje))
                            cursor.execute("update eliminacion_racimitos set estado=(%s) where estado=0 and viaje_id = (%s)",
                                           (respuestas[viaje], viaje))
                            conectar.commit()
    except Exception as e:
        print("ERROR SINCRONIZANDO VIAJES ELIMINADOS: " + repr(e))

    base = Get_trama(calibracions=True)
    try:
        if base is not 0:
            if base != "[]":
                response = Web_conex("calibracions", base)
                if response != 0:
                    print(response)
                    keys = []
                    flag = 0
                    respuestas = response[0]
                    for respuesta in response:
                        respuestas.update(respuesta)
                    for key in list(respuestas.keys()):
                        if key == 'error':
                            flag = 1
                        keys.append(key)
                    if flag == 1:
                        print("Error de request: ", respuestas['error'])
                    else:
                        cursor, conectar = Create_cursor()
                        for calibracion in keys:
                            # Actualizar la bandera de subido a los datos que se han ingresado a la web
                            cursor.execute("update validacions set estado=(%s) where estado=0 and validacion_id = (%s)", (respuestas[calibracion], calibracion))
                            conectar.commit()
    except Exception as e:
        print("ERROR SINCRONIZANDO: " + repr(e))


#### Funcion para filtrar picos y ondas cuadradas
def Filtro_picos(vector, vg=5, pd=0.8):

    it = len(vector)
    dif = vector[it - 1] - vector[it - 2]
    for i in range(2, it - 1):
        dif2 = vector[it - i] - vector[it - i - 1]
        if dif != 0 and dif2 != 0 and (abs(dif) > vg):
            if (abs(abs(dif) - abs(dif2))) / (max(abs(dif), abs(dif2))) < pd:
                for j in range(3, len(vector) - 1):
                    vector[j] = vector[3]
    #print "Vector Filtrado", vector
    return vector


#### Funcion para la validacion de conexion a internet
def Test_online():
    try:
        socket.setdefaulttimeout(3)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect(("8.8.8.8", 53))
        print('System Online')
        return 1
    except Exception as e:
        print('System Offline')
        print("ERROR: " + repr(e))
        return 0


#### Funcion para hacer shift a vectores
def Shift(vec, dato):
    vec = vec[1:]
    vec.append(dato)
    return vec


#### Funcion para log de datos
def Log_datos(dato, archivo):
    a = open(archivo, "a")
    a.write(str(dato)+'\n')
    a.close()



################# Funcion para la creacion de la tabla de parametros
def Conect_db_parametros():
    try:
        fecha = Actualizar_hora()
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')  # Conexión bases de datos
        cursor = conectar.cursor(cursor_factory=psycopg2.extras.DictCursor)  # Varibale Cursor con configuracion de generar el dict
        cursor.execute("CREATE TABLE IF NOT EXISTS parametros(parametro_id SERIAL PRIMARY KEY, parametro TEXT UNIQUE, fecha TEXT, valor TEXT)")  # Ejecución sentencia SQL
        conectar.commit()
        print('Conexion con tabla de parametros')
    except Exception as e:
        print("ERROR: " + repr(e))


#### Funcion para obtener parametro de la tabla estomas
def Get_parametro_estoma(parametro):
    try:
        cursor, conectar = Create_cursor(json=True)
        conectar.commit()
        cursor.execute("SELECT * FROM estomas")
        recs = cursor.fetchall()
        rows = [dict(rec) for rec in recs]
        for i in range(0, len(rows)):
                return rows[i][parametro]

    except Exception as e:
        print("ERROR OBTENIENDO PARAMETROS: " + repr(e))
        return 0


#### Funcion para obtener parametro de la base de datos
def Get_parametro(parametro):
    try:
        cursor, conectar = Create_cursor(json=True)
        conectar.commit()
        cursor.execute("SELECT * FROM parametros")
        recs = cursor.fetchall()
        rows = [dict(rec) for rec in recs]
        for i in range(0, len(rows)):
            existe = False
            if rows[i]['parametro'] == parametro:
                existe = True
                return rows[i]['valor']
        if not existe:
            print("El parametro ", parametro, "no existe")
            if(parametro == "Horas_dia_1") or (parametro == "Horas_dia_2") or (parametro == "Horas_dia_3"):
                return False
            else:
                return 1
    except Exception as e:
        print("ERROR OBTENIENDO PARAMETROS: " + repr(e))
        return 0


def Get_date_cero():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT fecha FROM parametros WHERE parametro = 'Horas_dia_1';")
    recs = cursor.fetchall()
    return recs[0][0]


def Get_info_cero():
    try:
        fecha_hoy = Actualizar_hora(dia=1)
        Horas_1 = Get_parametro("Horas_dia_1")
        if Horas_1 == False:
            Set_parametro("Horas_dia_1",0)
        else:
            Fecha_dia1= Get_date_cero()
            Date_dia1 = datetime.datetime.strptime(Fecha_dia1, '%Y-%m-%d %H:%M:%S')
            Date_dia1 = Date_dia1.date()

            if (fecha_hoy - Date_dia1) >= datetime.timedelta(days=1):
                Horas_2 = Get_parametro("Horas_dia_2")
                if Horas_2 == False:
                    Set_parametro("Horas_dia_2", Horas_1)
                    Set_parametro("Horas_dia_1", 0)
                else:
                    Horas_3 = Get_parametro("Horas_dia_3")
                    if Horas_3== False:
                        Set_parametro("Horas_dia_3", Horas_2)
                        Set_parametro("Horas_dia_2", Horas_1)
                        Set_parametro("Horas_dia_1", 0)
                    else:
                        tiempo_cero = float(Get_parametro_estoma("tiempo_cero"))
                        if float(Horas_1) + float(Horas_2) + float(Horas_3) >= tiempo_cero:
                            print("Enviando Correo_alerta_cero")
                            Correo_alerta_cero()
                        Set_parametro("Horas_dia_3", Horas_2)
                        Set_parametro("Horas_dia_2", Horas_1)
                        Set_parametro("Horas_dia_1", 0)

    except Exception as e:
        print("ERROR OBTENIENDO CERO DIARIO: " + repr(e))

#### Funcion para obtener ratio de la base de datos
def Get_ratio():
    try:
        cursor, conectar = Create_cursor(json=True)
        conectar.commit()
        cursor.execute("SELECT * FROM parametros")
        recs = cursor.fetchall()
        rows = [dict(rec) for rec in recs]
        for i in range(0, len(rows)):
            if rows[i]['parametro'] == 'ratio':
                return rows[i]['valor']

    except Exception as e:
        print("ERROR OBTENIENDO RATIO: " + repr(e))
        return 0


#### Funcion para obtener ratio de la base de datos
def Get_lote_id():
    try:
        cursor, conectar = Create_cursor(json=True)
        conectar.commit()
        cursor.execute("SELECT * FROM parametros")
        recs = cursor.fetchall()
        rows = [dict(rec) for rec in recs]
        for i in range(0, len(rows)):
            if rows[i]['parametro'] == 'lote_default':
                loteId = rows[i]['valor']
        return loteId

    except Exception as e:
        print("ERROR OBTENIENDO LOTE: " + repr(e))
        return 0


#### Funcion para ingresar o actualizar parametro en la base de datos
def Set_parametro(parametro, valor):
    fecha = Actualizar_hora()  # Actualizar la fecha a almacenar
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT * FROM parametros")
    recs = cursor.fetchall()
    rows = [dict(rec) for rec in recs]
    existe = False
    print(parametro)
    for i in range(0, len(rows)):
        if rows[i]['parametro'] == parametro:
            id = rows[i]['parametro_id']
            existe = True
    try:
        if existe:
            cursor.execute("update parametros set valor=%s, fecha=%s where parametro_id=%s;", (valor, fecha, id))
            print("Paramretro ", parametro, "Actualizado")
        else:
            cursor.execute("insert into parametros (parametro, fecha, valor) values (%s, %s, %s)", (parametro, fecha, valor))
            print("Paramretro ", parametro, "Actualizado")

        conectar.commit()
    except Exception as e:
        print("ERROR ACTUALIZANDO PARAMETRO ", parametro, ": ", repr(e))


################# Funcion para la conexion a la base de datos de backup
def Conect_db_backup():
    try:
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')  # Conexión bases de datos
        cursor = conectar.cursor(cursor_factory=psycopg2.extras.DictCursor)  # Varibale Cursor con configuracion de generar el dict
        cursor.execute("CREATE TABLE IF NOT EXISTS backups(backup_id SERIAL PRIMARY KEY, fecha DATE, tipo TEXT)")  # Ejecución sentencia SQL
        conectar.commit()
        print('Conexion con BD backups inicial RDY')
    except Exception as e:
        print("ERROR: " + repr(e))


#### Funcion para registrar los backups realizados por el equipo
def Insert_backup(fecha, tipo):
    cursor, conectar = Create_cursor()
    try:
        cursor.execute("insert into backups (fecha, tipo) values (%s, %s)", (fecha, tipo))
        print("Backup de " + tipo + " registrado ")
        conectar.commit()
    except Exception as e:
        print("ERROR INSERTANDO BACKUP: ", repr(e))


#### Funcion para obtener la fecha del ultimo backup realizado
def Get_last_backup(tipo):
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    if tipo == 'db':
        cursor.execute("SELECT max(fecha) FROM backups WHERE tipo='db';")
    elif tipo == 'log':
        cursor.execute("SELECT max(fecha) FROM backups WHERE tipo='log';")
    recs = cursor.fetchall()
    return recs[0][0]

def Get_subido():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT max(fecha) FROM racimitos WHERE estado=0;")
    recs = cursor.fetchall()
    if recs[0][0] is None:
        return True
    else:
        return False

#### Funcion para obtener la fecha del ultimo racimo
def Get_last_racimito():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT max(fecha) FROM racimitos")
    recs = cursor.fetchall()
    return recs[0][0]


#### Funcion para crea un nuevo viaje
def Crear_viaje():
    fecha = Actualizar_hora()
    cursor, conectar = Create_cursor()
    try:
        # se realiza la sentencia SQL registrando la bandera de subido en 0
        cursor.execute("insert into viajes (fecha, lote_id, estoma_id) values (%s, %s, %s)", (fecha, Get_lote_id(), Get_estomaid()))
        print("viaje creado")
    except psycopg2.Error as e:
        print("error al actualizar la bd interna: ", e)
    conectar.commit()


#### Funcion para obtener el ultimo viaje
def Get_last_viaje():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT max(viaje_id) FROM viajes")
    recs = cursor.fetchall()
    return recs[0][0]


def Set_fecha_final(viaje_id=0):
    try:
        cursor, conectar = Create_cursor(json=True)
        conectar.commit()
        if viaje_id == 0:
            cursor.execute("SELECT fecha from racimitos order by viaje_id desc, racimito_id desc limit 1")
        else:
            cursor.execute("SELECT max(fecha) from racimitos where viaje_id=(%s)", (viaje_id,))
        recs = cursor.fetchall()
        if not len(recs) > 0:
            print("Sin viajes previos")
            return 0
        fecha = str(recs[0][0])
        cursor, conectar = Create_cursor()
        if viaje_id == 0:
            cursor.execute("update viajes set fecha_final = (%s) where viaje_id = (SELECT viaje_id from viajes order by viaje_id desc limit 1)", (fecha, ))
        else:
            cursor.execute("update viajes set fecha_final = (%s) where viaje_id = (%s)", (fecha, viaje_id))
        conectar.commit()
        cursor.close()
        conectar.close()
    except Exception as e:
        print("ERROR INGRESANDO FECHA FINAL: ", repr(e))


def Cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]


def Web_noti_staff(subject, datos, timeout=6, intentos=1):
    print("Subject: ", subject)
    print("Datos a enviar: ", datos)
    SIOMAPPCREDENTIALS = "C0l0mb14S10m4"
    mydata = [('s', SIOMAPPCREDENTIALS), ('subject', subject), ('body', datos)]
    mydata = urllib.parse.urlencode(mydata)
    path = 'https://filtro.siomapp.com/1/correoAlertaStaff'
    header = {"Content-type": "application/x-www-form-urlencoded", "Authorization": "gS8Jdz24bJgevS9loGuiCW9R2IJXLcpdme8nQ8b88No"}
    ok = False
    for i in range(0, intentos):
        i += 1
        if not ok:
            try:
                page = requests.post(path, data=mydata, headers=header, timeout=timeout)
                return page.json()
                ok = True
            except Exception as e:
                print("ERROR EN LA CONEXION A SIOMA: " + repr(e))
                return 0
        else:
            break


def Web_noti_finca(subject, datos, timeout=6, intentos=1):
    print("Subject: ", subject)
    print("Datos a enviar: ", datos)
    SIOMAPPCREDENTIALS = "C0l0mb14S10m4"
    mydata = [('s', SIOMAPPCREDENTIALS), ('subject', subject), ('body', datos), ('estoma_nombre', ESTOMAID)]
    mydata = urllib.parse.urlencode(mydata)
    path = 'https://filtro.siomapp.com/1/correoAlertaFinca'
    header = {"Content-type": "application/x-www-form-urlencoded", "Authorization": "gS8Jdz24bJgevS9loGuiCW9R2IJXLcpdme8nQ8b88No"}
    ok = False
    for i in range(0, intentos):
        i += 1
        if not ok:
            try:
                page = requests.post(path, data=mydata, headers=header, timeout=timeout)
                return page.json()
                ok = True
            except Exception as e:
                print("ERROR EN LA CONEXION A SIOMA: " + repr(e))
                return 0
        else:
            break

def Correo_alerta_cero():
    SUBJECT_CERO = "Error en cero"
    BODY_CERO = "El equipo " + ESTOMAID + " presenta variación en el cero."
    print(Web_noti_staff(SUBJECT_CERO, BODY_CERO))
    Set_parametro("Correo_cero", 1)


def Viajes_sin_revisar():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    dia = Actualizar_hora(dia=1) - datetime.timedelta(days=1)
    cursor.execute("SELECT count(viaje_id) FROM viajes where revisado=0 and fecha > %s", (dia,))
    recs = cursor.fetchall()
    return recs[0][0]


def Set_barcadillero_default(viaje_id=0):
    try:
        cursor, conectar = Create_cursor(json=True)
        conectar.commit()
        cursor.execute("select barcadillero_id from viajes where barcadillero_id notnull order by viaje_id desc limit 1")
        recs = cursor.fetchall()
        barcadilleroId = recs[0][0]
        if viaje_id == 0:
            cursor.execute("update viajes set barcadillero_id=(%s) where viaje_id=(SELECT viaje_id from viajes order by viaje_id desc limit 1)", (barcadilleroId,))
        else:
            cursor.execute(
                "update viajes set barcadillero_id=(%s) where viaje_id=(%s)",
                (barcadilleroId, viaje_id))
        conectar.commit()
        cursor.close()
        conectar.close()
    except Exception as e:
        print("ERROR INGRESANDO USUARIO DEFAULT: ", repr(e))


def Set_operario_default(viaje_id=0):
    try:
        cursor, conectar = Create_cursor(json=True)
        conectar.commit()
        cursor.execute("select persona_id from viajes where persona_id notnull order by viaje_id desc limit 1")
        recs = cursor.fetchall()
        personaId = recs[0][0]
        if viaje_id == 0:
            cursor.execute(
                "update viajes set persona_id=(%s) where viaje_id=(SELECT viaje_id from viajes order by viaje_id desc limit 1)",
                (personaId,))
        else:
            cursor.execute(
                "update viajes set persona_id=(%s) where viaje_id=(%s)",
                (personaId, viaje_id))
        conectar.commit()
        cursor.close()
        conectar.close()
    except Exception as e:
        print("ERROR INGRESANDO OPERARIO DEFAULT: ", repr(e))



def Check_contenido_tablas(tablas):
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    contentTablas = False
    for tabla in tablas:
        cmd = "select * from " + str(tabla)
        try:
            cursor.execute(cmd)
            infoEnTablas = len(cursor.fetchall())
            if infoEnTablas > 0:
                contentTablas = True
            else:
                contentTablas = False
                break
        except Exception as e:
            print("ERROR CHECKEANDO CONTENIDO DE TABLAS, TABLA ", tabla, " :", e)
    return contentTablas


#Funcion para obtener la fecha de la ultima validacion
def Get_Last_Validacion():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT max(fecha) FROM validacions;")
    recs = cursor.fetchall()
    return recs[0][0]


#Funcion para insertar un nuevo registro de calibracion o validacion
def Insert_Calibracion(tipo, peso_validacion, error, ratio, correcto, barcadilleroCodigo, cero):
    fecha = Actualizar_hora()
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT persona_id FROM personas where codigo=(%s);", (barcadilleroCodigo,))
    recs = cursor.fetchall()
    barcadilleroId = recs[0][0]
    estomaId = Get_estomaid()
    cursor, conectar = Create_cursor()
    try:
        cursor.execute("insert into validacions (calibracion_on, fecha, error, ratio, peso,"
                       "correcto, persona_id, estoma_id, estado, cero) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       (tipo, fecha, error, ratio, peso_validacion, correcto, barcadilleroId, estomaId, 0, cero))
        print("Calibracion registrada")
        conectar.commit()
    except Exception as e:
        print("ERROR INSERTANDO CALIBRACION: ", repr(e))

#Funcion para obtener el cero de la ultima calibracion
def Get_Last_Cero():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("select cero from validacions order by fecha desc limit 1;")
    recs = cursor.fetchall()
    return float(recs[0][0])


def Get_wifi():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT ssid, psw FROM wifis;")
    recs = cursor.fetchall()
    return recs


def Save_wifi():
    wpa_supplicant_conf = "/etc/wpa_supplicant/wpa_supplicant.conf"
    # write wifi config to file
    f = open('wifi.conf', 'w')
    f.write('ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n')
    f.write('update_config=1\n')
    f.write('country=CO\n')
    f.write('\n')
    wifis = Get_wifi()
    print(wifis)
    for wifi in wifis:
        f.write('\n')
        f.write('network={\n')
        f.write('    ssid="' + wifi[0] + '"\n')
        f.write('    psk="' + wifi[1] + '"\n')
        f.write('}\n')
    f.close()

    cmd = 'sudo mv wifi.conf -f ' + wpa_supplicant_conf
    cmd_result = ""
    cmd_result = os.system(cmd)

def Get_estomaid():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT estoma_id FROM estomas;")
    recs = cursor.fetchall()
    estomaId = recs[0][0]
    return estomaId

def Get_barcadillero():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("select codigo from personas order by codigo desc limit 1")
    recs = cursor.fetchall()
    if len(recs) == 0:
        return 0
    else:
        return recs[0][0]
    conectar.commit()
    cursor.close()
    conectar.close()

def Get_vastago():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT vastago FROM fincas;")
    recs = cursor.fetchall()
    vastago = recs[0][0]
    return vastago
