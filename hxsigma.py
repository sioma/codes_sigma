# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#                Libreria de HX711 para Nukak                       #
#####################################################################
__author__ = 'cristianrojas'
######               Importacion de librerias                 #######
from hx711 import HX711
import funciones
ERROR_CERO_TOLERABLE = 0.05 # Porcentaje de error tolerable en el cero inicial

class HxSigma:
    #### Funcion de inicializacion de HX711
    def __init__(self, dout=10, sck=9, debug=False):
        try:
            self.hx = HX711(dout_pin=dout, pd_sck_pin=sck)
            for i in range(3):
                self.hx.set_debug_mode(debug)
                inicio = self.hx.reset()
                if inicio:
                    break
            cero = self.hx.zero(times=10)[1]
            if funciones.Get_Last_Validacion() is not None:
                ceroRef = funciones.Get_Last_Cero()
                errorTolerable = ceroRef*ERROR_CERO_TOLERABLE
                if abs(ceroRef-cero) > errorTolerable:
                    funciones.Set_parametro("error_cero", 1)
            self.ratio = float(funciones.Get_ratio())
            self.hx.set_scale_ratio(scale_ratio=self.ratio)
            print("HX711 Iniciado con exito")
        except Exception as e:
            print("Error iniciando HX711: " + repr(e))

    #####################################################################
    #####################################################################
    ######                        Funciones                       #######

    #### Obtener lecturas de peso
    def Get_lectura(self, intentosmax=3, mean=1):
        lectura = self.hx.get_weight_mean(mean)
        intentos = 0
        while not lectura and intentos <= intentosmax:
            lectura = self.hx.get_weight_mean(mean)
            intentos += 1
        if not lectura:
            return False
        else:
            return lectura

    ### Calibrar Sigma
    def Calibrar_sigma(self):

        result = self.hx.reset()
        self.hx.zero(times=10)
        # hx.set_debug_mode(True)
        if result:  # you can check if the reset was successful
            print('Ready')
            data = self.hx.get_data_mean(times=10)
            print(('Mean value from HX711 subtracted by offset: ' + str(data)))

        else:
            print('not ready')
        input('Poner peso conocido y despues pulsar Enter')
        data = self.hx.get_data_mean(times=10)
        if data != False:
            print(('Mean value from HX711 subtracted by offset: ' + str(data)))
            known_weight_grams = input('Ingrese el valor de peso conocido en Kg y pulse Enter: ')
            try:
                value = float(known_weight_grams)
                print((str(value) + ' Kg'))
            except ValueError:
                print(('Expected integer or float and I have got: ' \
                      + str(known_weight_grams)))

            ratio = data / value  # calculate the ratio for channel A and gain 64
            if ratio < 0:
                ratio = 2
            print('Ratio: ', ratio)
            funciones.Set_parametro("ratio", ratio)
            self.hx.set_scale_ratio(scale_ratio=ratio)  # set ratio for current channel
            print('Ratio is set.')
        else:
            raise ValueError('Cannot calculate mean value. Try debug mode.')

    def Get_peso(self):
        vectorFiltro = []
        lectura = 0
        for i in range(0, 8):
            vectorFiltro.append(self.hx.get_weight_mean(3))
        for i in range(0, 8):
            dato = self.hx.get_weight_mean(3)
            vectorFiltro = funciones.Shift(vectorFiltro, dato)
            vectorFiltro = funciones.Filtro_picos(vectorFiltro, pd=0.8)
            lectura = vectorFiltro[0]
        return lectura

    def reset(self):
        return self.hx.reset()

    def zero(self, times=10):
        return self.hx.zero(times=times)

    def get_data_mean(self, times=10):
        return self.hx.get_data_mean(times=times)

    def set_scale_ratio(self, ratio):
        return self.hx.set_scale_ratio(scale_ratio=ratio)

