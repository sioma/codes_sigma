# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#           Notificacion de apagado de procesos
#####################################################################
__author__ = 'cristianrojas'
###############         Importacion de librerias   ##################
import os
import time
import credentials
import funciones

estomaId = credentials.Get_Estoma_Info()[0]

try:
	print(funciones.Web_conex("fin", estomaId))
	time.sleep(0.5)
	os.system('sudo pkill python')
except Exception as e:
	print("Error: " + repr(e))
