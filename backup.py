# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#           Codigo para bakcup de bd y datos de log                 #
#####################################################################
__author__ = 'cristianrojas'
######               Importacion de librerias                 #######
import funciones

PERIODODB = 7
PERIODOLOG = 3

######  Inicializacion
funciones.Conect_db_backup()
fecha = funciones.Actualizar_hora(dia=1)
###### Backup de base de datos
lastBd = funciones.Get_last_backup('db')
if lastBd is None or (fecha - lastBd).days >= PERIODODB and funciones.Get_subido():
    print(funciones.Cmd_line("sudo tar --create --gzip --file='/home/pi/datos/db.tgz' '/home/pi/datos/db'"))
    print(funciones.Cmd_line("sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -c '\copy racimitos to '/home/pi/datos/db/racimitos.csv' with csv'"))
    funciones.Insert_backup(fecha, 'db')
    print(funciones.Cmd_line("sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -c 'delete from racimitos;'"))
    print(funciones.Cmd_line("sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -c 'delete from rechazos;'"))
    print(funciones.Cmd_line("sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -c 'delete from perfil_racimos;'"))
    print(funciones.Cmd_line("sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -c 'delete from viajes;'"))
else:
    print("Backup de DB se encuentra actualizado")

###### Backup de logs
lastLog = funciones.Get_last_backup('log')
if lastLog is None or (fecha - lastLog).days >= PERIODOLOG:
    print(funciones.Cmd_line("sudo tar --create --gzip --file='/home/pi/datos/logData.tgz' '/home/pi/datos/logData'"))
    funciones.Insert_backup(fecha, 'log')
    print(funciones.Cmd_line("sudo rm /home/pi/datos/logData/ConFiltro.txt /home/pi/datos/logData/SinFiltro.txt"))
else:
    print("Backup de logs se encuentra actualizado")

