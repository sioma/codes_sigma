import struct
import smbus
import sys
import time
import funciones

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(6, GPIO.IN)

cargador = GPIO.input(6)
estado_cargador = True

if cargador:     # if port 6 == 1
    print("---AC Power Loss OR Power Adapter Failure---")
    funciones.Set_parametro("conectada", 0)
    estado_cargador = False
else:                  # if port 6 != 1
    funciones.Set_parametro("conectada", 1)
    estado_cargador = True

def my_callback(channel):
    cargador = GPIO.input(6)
    if cargador:     # if port 6 == 1
        print("---AC Power Loss OR Power Adapter Failure---")
        funciones.Set_parametro("conectada", 0)
        estado_cargador = True
    else:                  # if port 6 != 1
        funciones.Set_parametro("conectada", 1)
        estado_cargador = False

GPIO.add_event_detect(6, GPIO.BOTH, callback=my_callback)

def readVoltage(bus):
    address = 0x36
    read = bus.read_word_data(address, 2)
    swapped = struct.unpack("<H", struct.pack(">H", read))[0]
    voltage = swapped * 1.25 / 1000 / 16
    return voltage


def readCapacity(bus):
    address = 0x36
    read = bus.read_word_data(address, 4)
    swapped = struct.unpack("<H", struct.pack(">H", read))[0]
    capacity = swapped / 256
    if capacity > 100:
        capacity = 100
    if capacity < 1:
        capacity = 1
    return capacity



