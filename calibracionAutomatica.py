# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#           Calibracion de bascula con interfaz de usario
#####################################################################
__author__ = 'cristianrojas'
###############         Importacion de librerias   ##################
import hxsigma as hs
import funciones
import os

DELTA_TOLERABLE = float(funciones.Get_parametro_estoma("tolerancia_validacion"))/1000 # Maximo error permitido en la validacion de peso
ARCHIVO_LOGS = "/home/pi/datos/logsCalibracion/logs.txt"
PATH_LOGS = "/home/pi/datos/logsCalibracion"
if not os.path.isdir(PATH_LOGS):
    os.makedirs(PATH_LOGS)
try:
    # Setear el valor inicial del estado_hx para dar aviso al front
    funciones.Set_parametro('estado_hx', 0)
    estado_hx = funciones.Get_parametro('estado_hx')
    ratio = funciones.Get_parametro('ratio')
    diferencia = 0
    peso = 0
    # Iniciar la clase HX
    hx = hs.HxSigma()
    error = 0
    correcto = 0
    validacion_poscalibracion = False
    save = False
    while not validacion_poscalibracion:
        estado_hx = int(funciones.Get_parametro('estado_hx'))
        while estado_hx != -1:
            save = False
            # Obtener actualizacion de estado por parte del front
            estado_hx = int(funciones.Get_parametro('estado_hx'))
            # si es 1 ya se encuentra el patron ubicado sobre el riel
            if estado_hx == 1:
                # se obtiene el tipo de proceso que se va a realizar
                tipoCal = int(funciones.Get_parametro('tipo_calibracion'))  ### 1 calibracion --- 0 validacion
                ### Validacion de inicio exitoso de hx
                result = hx.reset()
                cero = hx.zero(times=10)[1]
                #print cero
                if result:
                    print('Ready')
                    log = str(funciones.Actualizar_hora()) + ": Cero tomado correctamente"
                    funciones.Log_datos(log, ARCHIVO_LOGS)
                else:
                    print('not ready')
                    log = str(funciones.Actualizar_hora()) + ": El cero no se capturo correctamente, Posible error en HX"
                    funciones.Log_datos(log, ARCHIVO_LOGS)
                # Confirmar al front que se ha tomado el valor de referencia correctamente
                funciones.Set_parametro('estado_hx', 2)
            elif estado_hx == 3:
                patron = float(funciones.Get_parametro_estoma('patron'))
                if tipoCal == 1:
                    log = str(
                        funciones.Actualizar_hora()) + ": Se va a calibrar la bascula"
                    funciones.Log_datos(log, ARCHIVO_LOGS)
                    data = hx.get_data_mean(times=10)
                    ratio = data / 20  # calculate the ratio for channel A and gain 64 20kg es el patron por defecto
                    if ratio < 0:
                        ratio = 666666
                    print('Ratio: ', ratio)
                    log = str(
                        funciones.Actualizar_hora()) + ": Ratio :" + str(ratio)
                    funciones.Log_datos(log, ARCHIVO_LOGS)
                    funciones.Set_parametro("ratio", ratio)
                    hx.set_scale_ratio(ratio=ratio)  # set ratio for current channel
                    print('Ratio is set.')
                    funciones.Set_parametro('estado_hx', 4)
                elif tipoCal == 0:
                    log = str(
                        funciones.Actualizar_hora()) + ": Se va a validar la bascula"
                    funciones.Log_datos(log, ARCHIVO_LOGS)
                    peso = hx.Get_peso()
                    diferencia = abs(peso - patron)
                    log = str(
                        funciones.Actualizar_hora()) + ": Diferencia entre patron y pesaje: " + str(diferencia) + "Kg"
                    funciones.Log_datos(log, ARCHIVO_LOGS)
                    print("Diferencia entre patron y pesaje: ", diferencia, "kg")
                    print("Delta tolerable: ", DELTA_TOLERABLE)
                    log = str(
                        funciones.Actualizar_hora()) + ": Delta tolerable" + str(DELTA_TOLERABLE)
                    funciones.Log_datos(log, ARCHIVO_LOGS)
                    if diferencia > DELTA_TOLERABLE:
                        funciones.Set_parametro('estado_hx', 5)
                        funciones.Set_parametro("peso_validacion", peso)
                        error = 1
                        log = str(
                            funciones.Actualizar_hora()) + ": Se va a calibrar la bascula"
                        funciones.Log_datos(log, ARCHIVO_LOGS)
                    else:
                        funciones.Set_parametro('estado_hx', 4)
                        funciones.Set_parametro("peso_validacion", peso)
                        error = 0

        # Espera para validar que ya se quito el patron
        zero = patron
        while zero > 1:
            zero = hx.Get_peso()
        # Seteo de valor para almacenar en tabla calibracions
        if error == 1:
            correcto = 0
        elif error == 0:
            correcto = 1
        if not save:
            barcadilleroCodigo = funciones.Get_parametro('barcadillero_codigo')
            print("barcadillero: ", barcadilleroCodigo)
            funciones.Set_parametro('error', error)
            funciones.Insert_Calibracion(tipoCal, peso, str(diferencia), ratio, correcto, barcadilleroCodigo, cero)
            save = True
        if tipoCal == 1:
            funciones.Set_parametro('estado_hx', 0)
            funciones.Set_parametro('tipo_calibracion', 0)
        else:
            funciones.Set_parametro('estado_hx', -2)
            validacion_poscalibracion = True
except Exception as e:
    funciones.Log_datos(str(funciones.Actualizar_hora()) +"ERROR REALIZANDO CALIBRACION: " + repr(e), ARCHIVO_LOGS)
    print("ERROR REALIZANDO CALIBRACION: " + repr(e))









